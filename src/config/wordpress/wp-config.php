<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

$test_request_header = $_SERVER['HTTP_X_TEST_REQUEST'] ?? false;
$host = $_SERVER['HTTP_HOST'] ?? '';
$host_is_testing_site = 'testing.lndo.site' === $host;
$is_test_request = $test_request_header !== false;
$use_testing_db_credentials = $is_test_request || $host_is_testing_site;

if ( $host ) {
    define( 'WP_HOME', "https://$host" );
    define( 'WP_SITEURL', "https://$host" );
}

if ( $use_testing_db_credentials ) {
    define( 'DB_NAME', 'wordpress_testing' );
    define( 'DB_USER', 'wordpress_testing' );
    define( 'DB_PASSWORD', 'wordpress_testing' );
    define( 'DB_HOST', 'database_testing' );
} else {
    define( 'DB_NAME', 'wordpress' );
    define( 'DB_USER', 'wordpress' );
    define( 'DB_PASSWORD', 'wordpress' );
    define( 'DB_HOST', 'database' );
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '' );
define( 'SECURE_AUTH_KEY',   '' );
define( 'LOGGED_IN_KEY',     '' );
define( 'NONCE_KEY',         '' );
define( 'AUTH_SALT',         '' );
define( 'SECURE_AUTH_SALT',  '' );
define( 'LOGGED_IN_SALT',    '' );
define( 'NONCE_SALT',        '' );
define( 'WP_CACHE_KEY_SALT', '' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define( 'WP_DEBUG', true );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
