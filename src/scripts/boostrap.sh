#!/usr/bin/env bash
# bootstrap.sh file
# Copy Lando configuration files to the project root and configure them to be project specific.

# Calls to this script are expected to originate from the project root.
DIR=$(pwd)

# Copy the base lando file to the project root.
cp "$DIR/vendor/libertyconcepts/lando-wordpress-utils/src/config/lando/.lando.base.yml" "$DIR"

# Copy and rename the project specific lando file
cp "$DIR/vendor/libertyconcepts/lando-wordpress-utils/src/config/lando/example.lando.yml" "$DIR/.lando.yml"

# Grab the project name from the .env file, and then update the .lando.yml file to use the project name.
source "$DIR/.env"
echo "Loading environment from  $DIR/.env"
echo "Updating project name .lando.yml: $PROJECT_NAME"
sed -i '' -e "s/<project_name>/$PROJECT_NAME/g" $DIR/.lando.yml