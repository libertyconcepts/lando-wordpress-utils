#!/usr/bin/env bash
# setup.sh file
# This file is responsible for scaffolding out the necessary files on a new or legacy project so it can use the
# lando-wordpress-utils compose package.

CONFIG_DIR="./vendor/libertyconcepts/lando-wordpress-utils/src/config"

php ./vendor/bin/codecept bootstrap
cp $CONFIG_DIR/codeception/codeception.dist.yml .
cp $CONFIG_DIR/codeception/.env.testing .
cp $CONFIG_DIR/codeception/tests/* ./tests/