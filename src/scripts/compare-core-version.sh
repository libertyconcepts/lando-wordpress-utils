#!/usr/bin/env bash
# compare-core-version.sh file
# Manage the process of keeping the local and remote site versions of WordPress Core in sync.

DIRECTORY=$(cd `dirname $0` && pwd)
source "${DIRECTORY}/utils.sh"

function get_installed_local_core_version() {
  wp core version
}

function expected_core_version_matches_actual_core_version() {
  EXPECTED_LOCAL_VERSION=$WP_CORE_VERSION
  ACTUAL_LOCAL_VERSION=$(get_installed_local_core_version)
  return $(versions_match "$EXPECTED_LOCAL_VERSION" "$ACTUAL_LOCAL_VERSION")
}

function get_remote_core_version() {
  SSH_ADDRESS="${PROJECT_NAME}@${PROJECT_NAME}.ssh.wpengine.net"
  REMOTE_CORE_VERSION=$(echo $(ssh $SSH_ADDRESS "wp core version"))
  if [[ "$REMOTE_CORE_VERSION" == "" ]]; then
    show_error "There was an error connecting to WP Engine to determine the remote core version, aborting sync process."
    exit 1
  fi

  echo "$REMOTE_CORE_VERSION"
}

function versions_match() {
  local VERSIONS_MATCH=1
    if [[ "$1" == "$2" ]]; then
      VERSIONS_MATCH=0
    fi

    return $VERSIONS_MATCH
}

function compare_wordpress_core_versions() {
  LOCAL_CORE_VERSION=$1
  REMOTE_CORE_VERSION=$2
  show_message "Local WordPress Core is: $LOCAL_CORE_VERSION"
  show_message "Remote WordPress Core is: $REMOTE_CORE_VERSION"
  return $(versions_match "$LOCAL_CORE_VERSION" "$REMOTE_CORE_VERSION")
}

function prompt_sync_version_with_config() {
  USER_WANTS_TO_SYNC=1
  read  -n 1 -p "Do you want to sync your locally installed version of WordPress with the version specified in .lando.yml?: (y/n) " DO_VERSION_SYNC
  if [[ "$DO_VERSION_SYNC" == "y" ]]; then
    USER_WANTS_TO_SYNC=0
  fi
  return $USER_WANTS_TO_SYNC
}

function prompt_sync_version_with_production() {
  USER_WANTS_TO_SYNC=1
  read  -n 1 -p "Do you want to sync your locally installed version of WordPress with the Production version?: (y/n) " DO_VERSION_SYNC
  if [[ "$DO_VERSION_SYNC" == "y" ]]; then
    USER_WANTS_TO_SYNC=0
  fi
  return $USER_WANTS_TO_SYNC
}

function validate_installed_version_against_config() {
  show_message "Comparing locally installed version of WordPress Core with the version specified in config."
  expected_core_version_matches_actual_core_version
  INSTALLED_LOCAL_VERSION_MATCHES_VERSION_FROM_CONFIG=$?

  if [[ $INSTALLED_LOCAL_VERSION_MATCHES_VERSION_FROM_CONFIG -ne 0 ]]; then
    show_error "Your locally installed version does not match the config."
    prompt_sync_version_with_config
    DO_VERSION_SYNC=$?

    if [[ $DO_VERSION_SYNC -eq 0 ]]; then
        show_message "Ok, syncing the version of WordPress core with the version in your Lando config."
        wp core update --version="$WP_CORE_VERSION" --force
      else
        show_warning "I'll leave things alone, but please keep in mind you are developing on a different version of WordPress that what is specified in your project config."
      fi
  else
    show_success "Locally installed version matches version from config."
  fi
}

function validate_installed_version_against_remote() {
  show_message "Checking version of WordPress Core on $PROJECT_NAME"
  REMOTE_CORE_VERSION=$(get_remote_core_version)
  LOCAL_CORE_VERSION=$(get_installed_local_core_version)
  compare_wordpress_core_versions "$LOCAL_CORE_VERSION" "$REMOTE_CORE_VERSION"
  VERSIONS_IN_SYNC=$?

  if [[ $VERSIONS_IN_SYNC -ne 0 ]]; then
    show_error "Your local version of WordPress Core does not match the currently installed version on the Production site."
    prompt_sync_version_with_production
    DO_VERSION_SYNC=$?
    if [[ $DO_VERSION_SYNC -eq 0 ]]; then
      show_message "Ok, we'll sync that up for you."
      wp core update --version="$REMOTE_CORE_VERSION"
      show_message "Updating WP_CORE_VERSION in .lando.yml..."
      sed -i -e "s/WP_CORE_VERSION: $LOCAL_CORE_VERSION/WP_CORE_VERSION: $REMOTE_CORE_VERSION/g" "$DIRECTORY/../../../../../.lando.yml"
      show_success "Updates complete"
      exit 0
    else
      show_warning "I'll leave things alone, but please keep in mind you are developing on a different version of WordPress that what is currently on the live site."
    fi
  else
      show_success "Locally installed version matches the version running on Production."
  fi
}

validate_installed_version_against_remote
validate_installed_version_against_config