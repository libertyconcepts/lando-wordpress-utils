source ${DIRECTORY}/colors.sh

function show_message() {
  MESSAGE="$1"
  COLOR="$2"
  printf "${COLOR}\n>> ${MESSAGE}\n\n${NC}"
}

function show_error() {
  MESSAGE=$1
  show_message "$MESSAGE" "$RED"
}

function show_success() {
  MESSAGE=$1
  show_message "$MESSAGE" "$GREEN"
}

function show_warning() {
  MESSAGE=$1
  show_message "$MESSAGE" "$CYAN"
}