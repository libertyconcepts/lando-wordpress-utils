#!/usr/bin/env bash
# sync-db.sh file
# Run the WP CLI commands to perform a Migrate DB Pro database sync

DIRECTORY=$(cd `dirname $0` && pwd)
source "${DIRECTORY}/utils.sh"

function validate_production_url() {
  if [[ "$PRODUCTION_URL" == "" ]]; then
      show_error "The PRODUCTION_URL environment variable is not configured.  This could indicate you are missing a .env.licenses file.  If you have an .env.licenses file but PRODUCTION_URL is still not defined, you may need to run a 'lando rebuild'."
      exit 1
  fi
}

function get_sync_url() {
  if [[ "$BASIC_AUTH_LOGIN" != "" ]]; then
    SYNC_URL="https://$BASIC_AUTH_LOGIN:$BASIC_AUTH_PASS@$PRODUCTION_URL"
  else
    SYNC_URL=$PRODUCTION_URL
  fi

  echo "$SYNC_URL"
}

function get_admin_user_id() {
  ADMIN_USER_ID=$(wp user list --role=administrator --number=1 --format=ids)
  echo "$ADMIN_USER_ID"
}

function enable_migrate_db_plugin() {
  activate_plugin_if_inactive wp-migrate-db-pro-cli
  activate_plugin_if_inactive wp-migrate-db-pro
}


function configure_migrate_db_plugin_license() {
  ADMIN_USER_ID=$(get_admin_user_id)
  LICENSE_KEY=$(wp migratedb setting get license --user=$ADMIN_USER_ID)
  if [[ $LICENSE_KEY == "" ]]; then
    show_warning "No migration plugin license found, configuring now..."
    wp migratedb setting update license $WP_DB_MIGATE_PRO_LICENSE --user=$ADMIN_USER_ID
  else
    show_success "Migrate DB plugin License already configured."
  fi
}

function activate_plugin_if_inactive() {
  TARGET_PLUGIN=$1
  wp plugin is-active $TARGET_PLUGIN
  PLUGIN_IS_ACTIVE=$?
  if [[ $PLUGIN_IS_ACTIVE -ne 0 ]]; then
    show_warning "Plugin $TARGET_PLUGIN is not active, activating it now..."
    wp plugin activate $TARGET_PLUGIN
  else
    show_success "Plugin $TARGET_PLUGIN is already active."
  fi
}

function preflight_checks() {
  validate_production_url
  enable_migrate_db_plugin
  configure_migrate_db_plugin_license
}

function run_sync() {
  preflight_checks
  SYNC_URL=$(get_sync_url)
  wp migratedb pull $SYNC_URL $MIGRATION_KEY --find="$PRODUCTION_URL" --replace="$LANDO_APP_NAME.lndo.site"
}

run_sync
