#!/usr/bin/env bash
# build.sh file
# This file is responsible for downloading and installing Wordpress for both local development and local testing sites.

echo "Checking to see if WordPress is already installed..."
if wp core is-installed; then
    echo "WordPress is already installed, skipping build script."
    exit 0
fi

echo "WordPress not yet installed, downloading and installing it now..."
wp core download --version=${WP_CORE_VERSION}
cp ./vendor/libertyconcepts/lando-wordpress-utils/src/config/wordpress/wp-config.php ./wp-config.php
wp config shuffle-salts
wp config set WP_DEBUG true --raw
wp core install --url=${PROJECT_NAME}.lndo.site --title=${PROJECT_NAME} --admin_user=wordpress --admin_email=wordpress@example.com --admin_password=wordpress
